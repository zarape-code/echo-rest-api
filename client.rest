GET http://localhost:1323 HTTP/1.1

###
POST http://localhost:1323/login HTTP/1.1
Content-Type: application/json

{
    "email": "ethien.salinas@gmail.com",
    "password": "qwerty"
}

###
POST http://localhost:1323/users/ HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjA2Mjg5MzQ1LCJuYW1lIjoiIn0.9PiRuZ6UFsqySUUGX6m7B37NdW8JizaPZSYHOqIrz9s

###
GET http://localhost:1323/users/ HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjA2Mjg5MzQ1LCJuYW1lIjoiIn0.9PiRuZ6UFsqySUUGX6m7B37NdW8JizaPZSYHOqIrz9s

###
GET http://localhost:1323/users/333 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjA2Mjg5MzQ1LCJuYW1lIjoiIn0.9PiRuZ6UFsqySUUGX6m7B37NdW8JizaPZSYHOqIrz9s

###
PUT http://localhost:1323/users/333 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjA2Mjg5MzQ1LCJuYW1lIjoiIn0.9PiRuZ6UFsqySUUGX6m7B37NdW8JizaPZSYHOqIrz9s

###
DELETE http://localhost:1323/users/333 HTTP/1.1
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6dHJ1ZSwiZXhwIjoxNjA2Mjg5MzQ1LCJuYW1lIjoiIn0.9PiRuZ6UFsqySUUGX6m7B37NdW8JizaPZSYHOqIrz9s

###