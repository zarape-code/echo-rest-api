# Echo REST API

API con [Go](https://golang.org/) y el framework [echo](https://echo.labstack.com/) [más info...](http://blog.zarape.io/)

## Iniciar el proyecto
Generar el archivo `.env` con las keys necesarias
```sh
$ mv .env-example .env
```
Arrancar el proyecto en modo desarrollo (no cuenta con hot reload)
```sh
$ go run server.go
```
Compilar proyecto para `prod` 
```sh
$ go build -o echo-rest-api.e
```
Ejecutar binario 
```sh
$ ./echo-rest-api.e
```
Construir y ejecutar binario para probar
```sh
$ go build && ./echo-rest-api.e
```

## Estructura del proyecto
```sh
\                       # archivos de configuración
|-server.go             # archivo pricipal del proyecto
|-handlers/             # agrupador del código fuente
    |-auth.go           # generador de JWT
    |-user_struct.go    # estructura para modelar el json de entrada
    |-users.go          # lógica del CRUD para users
```
## Rutas protegidas
La configuración se hace en el archivo `server.go` y se ayuda del [JWT Middleware](https://echo.labstack.com/middleware/jwt) existente en [echo](https://echo.labstack.com/) para validarlo
```go
// creación de grupo y uso de middleware para requerir JWT
r := e.Group("/users")
r.Use(middleware.JWT([]byte(os.Getenv("JWT_SECRET"))))
```
La lógica para generarlo  está en el archivo `auth.go`

## Cliente
El proyecto puede ser probado usando las llamadas registradas en el archivo `client.rest` con ayuda de la extensión [REST Client](https://github.com/Huachao/vscode-restclient)

## Tecnología empleada
* [Go](https://golang.org/) - Lenguaje de programación
* [echo](https://echo.labstack.com/) - Web framework
* [GoDotEnv](https://github.com/joho/godotenv) - carga de variables de entorno en archivo `.env`

## Extensiones de VSCode usadas
* [Go](https://marketplace.visualstudio.com/items?itemName=golang.Go) - Soporte para Go en VSCode
* [REST Client](https://github.com/Huachao/vscode-restclient) - Alternativa a Postman para crear llamadas a la API
* [OpenAPI (Swagger) Editor](https://marketplace.visualstudio.com/items?itemName=42Crunch.vscode-openapi) - Soporte para la especificación OpenAPI (antes Swagger Specification)
___
### Autor
Creado por [Ethien Salinas](https://www.linkedin.com/in/ethiensalinas/) para zarape.io 🇲🇽